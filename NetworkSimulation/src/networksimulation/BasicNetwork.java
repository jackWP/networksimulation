package networksimulation;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.text.ParseException;
import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.Random;
import java.util.Scanner;
import java.time.format.DateTimeFormatter;
import java.util.PriorityQueue;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Version: 1
 * Date Created: 02/01/2020
 * Author: Jack Warner-Pankhurst, University of East Anglia, 100199337
 * Description: This class acts as where all the other classes come together and
 * are used to fully simulate a network throughout the simulation time. It uses
 * the BasicNodes and Connections to create a topology, creates Incidents to 
 * simulate changes in the network, and creates Commands based on the users 
 * input to query the state of the network throughout the simulation. The 
 * results of the commands are then saved to REPORT.TXT for analysis by the user
 */

public class BasicNetwork extends Network
{
    /**
     * Stores the end time of the simulation as a LocalDateTime Object
     */
    private final LocalDateTime endTime;
    
    /**
     * Random object that is used throughout the system, seeded when the network
     * is created
     */
    private final Random random;
    
    /**
     * HashMap that used the ID of the node as a key, and a BasicNode as the 
     * value. Keeps track of all the nodes in the network for conversion to a
     * Graph
     */
    private HashMap<Integer, BasicNode> nodes;
    
    /**
     * Same as the Node HashMap but for connections, also used in the conversion
     * to a Graph
     */
    private HashMap<Integer, BasicConnection> connections;
    
    /**
     * PriorityQueue of Commands sorted by the execution time fo the commands so 
     * they can be pulled from the queue in proper order of execution
     */
    private PriorityQueue<Command> commandList;
    
    /**
     * Same as the Command PriorityQueue but for incidents. Each list is pulled
     * from based on the sooner executing object
     */
    private PriorityQueue<Incident> incidentList;
    
    /**
     * StringBuilder that is added to every time a command is executed. Is saved
     * once all commands are executed to write the results to the RESULTS.TXT 
     * file
     */
    private StringBuilder report;
    
    /**
     * The WeightedGraph of the network. This uses the current state of the 
     * nodes and connections to find the shortest path using Dijkstras algorithm
     * from the source node to the target of the command
     */
    private Graph networkGraph;
    
    /**
     * This is where all commands start from, currently randomly selected from 
     * all nodes
     */
    private GraphNode sourceNode;
    
    /**
     * Sets the total number of connections to be made
     */
    private final int NUMBER_OF_CONNECTIONS = 1000;
    
    /**
     * Sets the number of nodes to be made
     */
    private final int NUMBER_OF_NODES = 300;
    
    /**
     * Sets the number of unique incidents to be made
     */
    private final int NUMBER_OF_INCIDENTS = 1000;
    
    /**
     * Base constructor for a BasicNetwork object. Returns a basicNetwork object
     * that can be run using the set parameters at the top of this file
     * @param seed Integer seed to set the Random object to
     */
    public BasicNetwork(int seed)
    {
        super(seed);
        //localdatetime is immutable so have to capture results of add 
        //using temporary var
        LocalDateTime temp = LocalDateTime.now();
        // add 7 days to simualtion to simulate 1 week, can be easily changed to
        //be based on user input
        this.endTime = temp.plusDays(7);                         
        nodes = new HashMap();
        connections = new HashMap();
        commandList = new PriorityQueue<>();
        incidentList = new PriorityQueue<>();
        report = new StringBuilder();
        random = new Random(seed); //set seeded random number generator
    }
    
    /**
     * This method generates all of the random elements of the network. First 
     * nodes, then connections, then incidents
     */
    @Override
    public void generateNetwork() 
    {
        //Generate 300 nodes
        for(int ID = 0; ID < NUMBER_OF_NODES; ID++)
        {
            int speed = this.RandomInteger(0, 50);
            this.addNode(new BasicNode(ID,speed));
        }       
        
        int connection1ID = 0;
        int connection2ID = 0;
        //Generate 1000 connections
        for (int i = 0; i < NUMBER_OF_CONNECTIONS; i++) 
        {
            int start = this.RandomInteger(0,NUMBER_OF_NODES);
            //make sure no node has more than 10% of all connections,
            while(this.nodes.get(start).getNumberOfConnections() > 
                    this.NUMBER_OF_CONNECTIONS*0.2)
            {
                start = this.RandomInteger(0,NUMBER_OF_NODES);
            }
            
            int end = this.RandomInteger(0,NUMBER_OF_NODES);
            while(end == start)
            {
                end = this.RandomInteger(0,NUMBER_OF_NODES);
            }
            //generate speed
            int speed = this.RandomInteger(0,101);
            //create the connection to the start node
            BasicConnection connection = 
                    new BasicConnection(connection1ID++,start,end,speed);
            //create the connection to the end node in the other direction to
            //ensure all connections are two ways
            BasicConnection connection2 = 
                    new BasicConnection(connection2ID++,end,start,speed);
            //add the connections to the respective nodes
            BasicNode n = this.nodes.get(start);
            n.addConnection(connection);
            n = this.nodes.get(end);
            n.addConnection(connection2);
            //add the connections to the connections list
            this.connections.put(connection1ID, connection);
            this.connections.put(connection2ID, connection2);
        } 
    }
    
    /**
     * This method takes all fo the generated elements after the commands gave 
     * been read, and runs the commands and incidents on the network over the
     * set simulation time. This works by executing either the soonest command
     * or incident. Before every command is executed, the Graph is rebuilt so 
     * that the shortest path algorithm can recalculate the distances and paths
     * based on the changes made by incidents
     */
    @Override
    public void runNetwork()
    {
        this.generateNetwork(); //generate the reabdom elements of the network
        
        //check to make sure that nodes are successfully generated, otherwise 
        //exit the method, then the program to ensure no crashes
        try
        {
            this.networkGraph = this.convertNetworkToGraph();
        }
        catch(EmptyNodeHashMapException error)
        {
            System.out.println("An error occured while building the network graph:");
            System.out.println(error);
            System.out.println("Exiting.");
            return;
        }
        
        //randomly assign source node, easily changed to a user inputted one
        this.sourceNode = this.networkGraph.getGraphNodes().get(
                this.RandomInteger(0, NUMBER_OF_NODES)); 
        
        //genertate the network incidents from the parameters at the top of 
        //the file
        this.generateIncidents();
        //read the command file the create the appropriate number and type of
        //commands
        this.readCommandFile();
        //execute the commands and incidents to run the simulation
        try 
        {
            this.runIncidentsAndCommands();
        } 
        catch (EmptyNodeHashMapException ex) 
        {
            System.out.println(ex);
        }
        //save results of the commands to write to file
        this.saveReport();
    }
    
    /**
     * This method is the actual simulation part of the system.It executes
     * the commands and incidents in order of time (soonest relative to the 
     * simulation end first). The results of the commands are saved for the 
     * report
     * @throws networksimulation.EmptyNodeHashMapException If the nodes HashMap
     * is empty, throw an exception to ensure commands or incidents do not run 
     * on non-existant objects
     */
    public void runIncidentsAndCommands() throws EmptyNodeHashMapException
    {
        //while there are commands
        while(!this.commandList.isEmpty())
        {
            //if no incidents are left, just run the last commands
            if(this.incidentList.isEmpty())
            {
                this.convertNetworkToGraph();
                //for each of the commands, update the stored graph
                for(Command c : this.commandList)
                {                       
                    if(c instanceof PingCommand)
                    {
                        ((PingCommand) c).setGraph(this.networkGraph);
                    }
                    else if(c instanceof TraceCommand)
                    {
                        ((TraceCommand) c).setGraph(this.networkGraph);
                    }
                    else
                    {
                        break;
                    }
                    //then run the commands
                    c.runCommand();
                }
            }
            else
            {   //if a command need to be executed sooner, rebuild grapgh and
                //execute it and remove it from queue
                if(this.commandList.peek().getExecutionTime().isBefore(
                        this.incidentList.peek().getExecutionTime()))
                {
                    this.convertNetworkToGraph();
                    Command com = this.commandList.poll();
                    this.addToReport((String)com.runCommand());
                }
                //otherwise execute next incidents, ramove it from queue
                else
                {
                    Incident ins = this.incidentList.poll();
                    ins.runIncident();
                }
            }           
        }
        System.out.println("All commands run, saving results to Report.txt...");
    }
    
    /**
     * This method takes the network objects list of nodes and connections and 
     * builds a weighted graph from them for use with the shortest path 
     * algorithm
     * @return Graph object that represents the nodes and connections stored
     * in the network
     * @throws EmptyNodeHashMapException In case no nodes have been created, 
     * throw exception to ensure commands or incidents are not run
     */
    public Graph convertNetworkToGraph() throws EmptyNodeHashMapException
    {
        Graph convertedGraph = new Graph();
        
        //If there are no keys i.e. nodes, throw exception
        if(!this.nodes.keySet().isEmpty())
        {
            //else add every noeds to the graph
            for(int id : this.nodes.keySet())
            {
                convertedGraph.addNode(new GraphNode(this.nodes.get(id)));
            }
            //then add every connection for each node
            for(int id : this.connections.keySet())
            {
                BasicConnection c = this.connections.get(id);
                //add the destination of every connection to the desitation node
                //to the start node of the connection
                GraphNode dest = convertedGraph.GraphNodeFromNetworkNode(
                        this.nodes.get(c.getStartNodeID()));
                dest.addDestination(convertedGraph.GraphNodeFromNetworkNode(
                        this.nodes.get(c.getEndNodeID())), c.getConnectionSpeed());
            }
            
        }
        else
        {
            throw new EmptyNodeHashMapException("There are no nodes to create the graph.");
        }
        return convertedGraph;
    }
    
    /**
     * Adds a BasicNode to the HashMap of nodes
     * @param n The BasicNode to be added
     */
    public void addNode(BasicNode n)
    {
        this.nodes.put(n.getID(), n);
    }
    
    /**
     * Adds and BasicCpnnection to the HashMap of connections
     * @param c The BasicConnection to be added
     */
    public void addConnection(BasicConnection c)
    {
        this.connections.put(c.getConnectionID(),c);
    }
    
    /**
     * Uses the StringBuilder in the network to create and save the report, 
     * which contains all the formatted results of the commands
     */
    public void saveReport()
    {
        File file = new File("REPORT.txt");
        try (BufferedWriter writer = new BufferedWriter(new FileWriter(file))) 
        {
        writer.write(this.report.toString());
        } 
        catch (IOException ex) 
        {
            Logger.getLogger(BasicNetwork.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    /**
     * Helper method to use the networks seeded Random object easily
     * @param lowerBound lower bound for the random number
     * @param upperBound upper bound for the random number
     * @return Random Integer between the lower (inclusive) and upper 
     *  bounds(exclusive) 
     */
    public int RandomInteger(int lowerBound,int upperBound)
    {
        return random.ints(lowerBound,(upperBound)).findFirst().getAsInt();
    }
    
    /**
     * Generates the incidents to occur on the network over the simulation time.
     * The number of unique incidents is set at the top of the file. The actual
     * number will be significantly higher as the incidents that occur 
     * consistently happen every hour to a node over the whole simulation
     */
    public void generateIncidents()
    {
        int numOfRandomConnectionIncidents = this.NUMBER_OF_INCIDENTS/2; //500
        int numOfRandomNodeIncidents = this.NUMBER_OF_INCIDENTS/2; //500

        
        //generates 500 randomly assigned connection incidents
        for (int connectionIncidentID = 0; connectionIncidentID < numOfRandomConnectionIncidents; connectionIncidentID++) 
        {
            int target = this.RandomInteger(0,this.NUMBER_OF_CONNECTIONS-1);
            LocalDateTime executionTime = this.getSimulationStartTime().plusHours(this.RandomInteger(0,336));
            int change = this.RandomInteger(0,10);
            
            // 50% chance that speed goed up or down (this is checked in BasicConnection to ensure no negative speeds)
            
            if(this.RandomInteger(0,1) == 0)
            {
                change = -change;
            }
            
            this.addIncident(new ConnectionIncident(connectionIncidentID, executionTime, change, target, this.getConnectionHashMap()));
        }
        
        //generates a random set of consistent connection incidents that cause a connection to consistently go up and down in speed over a day, and the whole simulation
        for (int targetNodeID = 0; targetNodeID < this.NUMBER_OF_CONNECTIONS; targetNodeID+= this.RandomInteger(1,4)) //between 500 and 125 nodes
        {      
            int change = 8;
            boolean increase = false;
            LocalDateTime executionTime = this.getSimulationStartTime().plusHours(1);
            
            while(executionTime.isBefore(this.getSimulationEndTime()))
            {
                if(executionTime.getHour() == 8)
                {
                    increase = true;
                }
                else if(executionTime.getHour() == 20)
                {
                    increase = false;
                }
                
                if(increase)
                {
                    change++;
                }
                else
                {
                    change--;
                }
                
                this.addIncident(new ConnectionIncident(targetNodeID, executionTime, change, targetNodeID, this.getConnectionHashMap()));
                executionTime = executionTime.plusHours(1);
            }
        }
        
        //generates randomly assigned node processing incidents 
        for (int i = 0; i < numOfRandomNodeIncidents; i++) 
        {
            int target = this.RandomInteger(0,this.NUMBER_OF_NODES-1);
            LocalDateTime executionTime = this.getSimulationStartTime().plusHours(this.RandomInteger(0,336));
            int change = this.RandomInteger(0,20);
            
            // 50% chance that speed goes up or down (this is checked in BasiConnection to ensure no negative speeds)
            if(this.RandomInteger(0,1) == 0)
            {
                change = -change;
            }
            
            this.addIncident(new NodeProcessingIncident(i, executionTime, change, target, this.getNodeHashMap()));
        }
    }
    
    /**
     * Adds and Incident to the PriorityQueue of incidents
     * @param incident incident to add
     */
    private void addIncident(Incident incident)
    {
        this.incidentList.add(incident);
    }
    
    /**
     * Returns the current HashMap of Connections
     * @return HashMap<Integer, BasicConnection> of the current connections
     */
    private HashMap<Integer, BasicConnection> getConnectionHashMap()
    {
        return this.connections;
    }
    
    /**
     * Returns the current HashMap of Nodes
     * @return HashMap<Integer, BasicNode> of the current Nodes
     */
    private HashMap<Integer, BasicNode> getNodeHashMap()
    {
        return this.nodes;
    }
    
    /**
     * Reads the command file and performs string splitting ans casting to 
     * read the commands and create the correct command with parameters, and 
     * checks for incorrect commands and ignores them. Also ignores lines with \
     * at the start like a comment
     */
    @Override
    public void readCommandFile() 
    {
        System.out.println("Reading commands file....");
        try
        {
            FileInputStream fis = new FileInputStream("commands.txt");
            Scanner sc = new Scanner(fis);
            int lineNum = 1;
            int lastCommandID = 0;
            String[] splitCommand;
            while(sc.hasNextLine())
            {
                String command = sc.nextLine();
                
                if(command.startsWith("/"))
                {
                    System.out.println("Command ignored, line " + lineNum);
                    lineNum++;
                }
                else
                {
                    try
                    {
                        splitCommand = checkCommandFormat(command,lineNum);
                        char firstChar = splitCommand[0].charAt(0);
                        DateTimeFormatter formattedDate = DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm:ss");
                        switch(firstChar)
                        {
                            //---------------------------------- PING 
                            case 'p': 
                                try
                                {
                                    LocalDateTime commandExecutionTime = this.getSimulationStartTime();

                                    while(commandExecutionTime.isBefore(this.getSimulationEndTime()))
                                    {   
                                        commandExecutionTime = calculateExecutionTime(commandExecutionTime,splitCommand[2]);

                                        PingCommand ping = this.createPingCommand(splitCommand, lastCommandID++, commandExecutionTime);

                                        System.out.println("Ping command created, targetting node ID "+splitCommand[1] + " at time " + formattedDate.format(ping.getExecutionTime()));
                                        this.addCommand(ping);
                                    }
                                    break;
                                }
                                catch(ParseException error)
                                {
                                    System.out.println("The ping command on line " + lineNum + " has an incorrect argument type.");
                                    System.out.println(error);
                                    break;
                                }          
                            //-------------------------------------------------- TRACE
                            case 't': 
                                try
                                {
                                    LocalDateTime commandExecutionTime = this.getSimulationStartTime();

                                    while(commandExecutionTime.isBefore(this.getSimulationEndTime()))
                                    {   
                                        commandExecutionTime = calculateExecutionTime(commandExecutionTime,splitCommand[2]);

                                        TraceCommand trace = this.createTraceCommand(splitCommand, lastCommandID++, commandExecutionTime);

                                         System.out.println("Trace command created, targetting node ID " +splitCommand[1] + " at time " + formattedDate.format(trace.getExecutionTime()));
                                        this.addCommand(trace);
                                    }
                                    break;
                                }
                                catch(ParseException error)
                                {
                                    System.out.println("The trace command on line " + lineNum + " has an incorrect argument type.");
                                    System.out.println(error);
                                    break;
                                }
                            //-------------------------------------------------- DEFAULT
                            default: 
                                System.out.println("Unrecognised command on line " + lineNum + ". This could be an incorrect switch or argument type.");
                        }
                        lineNum++; // increment line count before next loop
                    }
                    catch(IncorrectArgumentNumberException error)
                    {
                        System.out.println(error);
                    }
                }
            }
        }
        catch(FileNotFoundException error)
        {
            System.out.println("File not found: " + error);
        }
    }
    
    /**
     * Helper method to get the execution time of a command from a string
     * @param start This is when the simulation starts, so commands are relative 
     * to this time
     * @param splitString An array containing the parts of the string with H:M:S
     * @return A LocalDateTimeObject that is when the command will execute
     */
    private LocalDateTime calculateExecutionTime(LocalDateTime start, String splitString)
    {
        String[] timeAmounts = splitString.split(":"); 
        
        start = start.plusHours(Integer.parseInt(timeAmounts[0]));
        start = start.plusMinutes(Integer.parseInt(timeAmounts[1]));
        start = start.plusSeconds(Integer.parseInt(timeAmounts[2]));
        
        return start;
    }
    
    /**
     * Helper method to check the format of a command based on the number of 
     * provided arguments
     * @param command The string to split into arguments
     * @param lineNum to provide feedback in case a malformed argument is found
     * @return A string containing a correctly formatted command
     * @throws IncorrectArgumentNumberException Throws an exception to be 
     * handled if a malformed command is found
     */
    private String[] checkCommandFormat(String command, int lineNum) throws IncorrectArgumentNumberException
    {
        //split so any number of spaces count as a seperator
       String[] split = command.split("\\s+"); 
       
       if(split.length != 3) //if there are more or less than 3 arguments, throw excepetion
       {
           throw new IncorrectArgumentNumberException("Incorrect number of arguments line " + lineNum + ". Expected 3, found " + split.length + " arguments.");
       }
       else //othwewise, command is correctly formatted and returns
       {
           return split;
       }
    }
    
    /**
     * Adds and String to the report StringBuilder
     * @param s String to add
     */
    public void addToReport(String s)
    {
        this.report.append(s);
    }
    
    /**
     * Method to help create Ping commands once they are read from a file
     * @param splitCommand All the arguments of the command
     * @param commandID The ID of the command
     * @param commandExecutionTime When the command will execute
     * @return A Ping command with the above fields
     * @throws ParseException If part of the arguments do not parse i.e. letter 
     * instead of a number is found, throw an exception so malformed command
     * object is not created
     */
    private PingCommand createPingCommand(String[] splitCommand, int commandID, LocalDateTime commandExecutionTime) throws ParseException
    {   //parse exception if argument is non-integer
        int endNodeID = Integer.parseInt(splitCommand[1]); 
        //get the targeted end node
        GraphNode endNode = this.networkGraph.GraphNodeFromNetworkNode(
                this.nodes.get(endNodeID));

        return new PingCommand(commandID, commandExecutionTime, 
                this.getSourceNode(), endNode, this.networkGraph);
    }
    
    /**
     * Method to help create Ping commands once they are read from a file
     * @param splitCommand All the arguments of the command
     * @param commandID The ID of the command
     * @param commandExecutionTime When the command will execute
     * @return A Ping command with the above fields
     * @throws ParseException If part of the arguments do not parse i.e. letter 
     * instead of a number is found, throw an exception so malformed command
     * object is not created
     */
    private TraceCommand createTraceCommand(String[] splitCommand, int commandID, LocalDateTime commandExecutionTime) throws ParseException
    {
        int endNodeID = Integer.parseInt(splitCommand[1]); //parse exception if argument is non-integer
        
        GraphNode endNode = this.networkGraph.GraphNodeFromNetworkNode(this.nodes.get(endNodeID));
        
        return new TraceCommand(commandID, commandExecutionTime, this.getSourceNode(), endNode, this.networkGraph);
    }
    
    /**
     * Adds a Command to the PriorityQueue of commands
     * @param c Command to be added
     */
    public void addCommand(Command c)
    {
        this.commandList.add(c);
    }
    
    /**
     * Returns the number of Nodes in the network
     * @return Integer as the number of nodes in the network
     */
    public int size()
    {
        return this.nodes.keySet().size();
    }
    
    /**
     * Sets the source node of the network
     * @param source GraphNode to set as the source node
     */
    public void setSourceNode(GraphNode source)
    {
        this.sourceNode = source;
    }
    
    /**
     * Gets the current source node
     * @return GraphNode that is the current source node
     */
    public GraphNode getSourceNode()
    {
        return this.sourceNode;
    }
    
    /**
     * Gets the simulation end time
     * @return LocalDateTime as he end time of the simulation
     */
    public LocalDateTime getSimulationEndTime()
    {
        return this.endTime;
    }
    
    /**
     * Gets the graph currently in the network
     * @return Graph as the current Graph in the network
     */
    public Graph getNetworkGraph()
    {
        return this.networkGraph;
    }
    
    /**
     * Creates a formatted string of the current lists of commands and nodes
     * in the network. not recommended to use this on larger networks
     * @return String representation of the network nodes and connections
     */
    @Override
    public String toString()
    {
        StringBuilder s = new StringBuilder();
        s.append("Nodes in network: \n");
        for (int i = 0; i < this.nodes.size()-1; i++) 
        {
            s.append(this.nodes.get(i).toString());
            s.append("\n");
        }
        
        s.append("Connections in network: \n");
        
        for (int i = 0; i < this.connections.size()-1; i++) 
        {
            s.append(this.connections.get(i).toString());
            s.append("\n");
        }
        
        return s.toString();
    }
    
    /**
     * Test harness for the whole system
     * @param args arguments passed in the command line
     */
    public static void main(String[] args) 
    {   
        BasicNetwork n = new BasicNetwork(10099337);
        n.runNetwork();
        
        System.out.println(n.getNetworkGraph().getGraphNodes().get(22));
        //System.out.println("Shortest path size: " +n.getNetworkGraph().getGraphNodes().get(22).getShortestPath().size());
        //System.out.println("Distance: " + n.getNetworkGraph().getGraphNodes().get(22).getDistance());
       
    }  
}