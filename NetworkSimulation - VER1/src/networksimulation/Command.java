package networksimulation;
import java.util.Calendar;

/**
 * Version: 1
 * Date Created: 02/01/2020
 * Author: Jack Warner-Pankhurst, University of East Anglia, 100199337
 * Description: Commands are used by the user to investigating the network and 
 * its properties at specified times. Commands are generated by reading in a 
 * text file of user written commands (with switches and variables) and then 
 * generating the appropriate commands and storing them in the network object 
 * for execution later. 
 * 
 * Methods to override in this object:
 * runCommand
 * addToReport (may not need this)
 */
public abstract class Command 
{
    /**
     * Integer id to uniquely identify each command in the list stored in the 
     * Network 
     */
    private final int commandID;
    
    /**
     * Calendar object that stores a set time from the networks creation for the 
     * command to be executed when it is processed by the network. 
     */
    private final Calendar executionTime;
    
    /**
     * Command constructor for initialising the object with an unique id, and a
     * specified time (stored within a Calendar object) for the command to 
     * be executed at.
     * Commands should never be initialised without an id or an execution time,
     * this should avoid commands with no id or execution time existing on the 
     * network, which could cause instabilities.
     * @param id Integer id to uniquely identify the command on the network
     * @param c Calendar object that stores the execution time for the command,
     * relative to the networks initialisation. 
     */
    Command(int id, Calendar c)
    {
        this.commandID = id;
        this.executionTime = c;
    }
    
    /**
     * Method to return the execution time of the command stored within the 
     * Calendar object
     * @return Calendar object containing the execution time of the command
     */
    public Calendar getExecutionTime()
    {
        return this.executionTime;
    }
    
    /**
     * This method should be overridden to implement the desired implementation
     * of the command. Commands should be used by the user to investigate the 
     * networks in terms of topology, and the status of connections and nodes. 
     * Commands will be created by reading in text from a text file and creating
     * the appropriate commands with the correct execution times. 
     * Commands also need to add to the command log report, summarising the ------------ DEBATING THIS
     * action taken and the returned results.  
     */
    public void runCommand()
    {
        
    }
    
    /** Debating this
     * Once a command has finished executing its code, the results need to be 
     * added to the command log so that the user can analyse the data for 
     * themselves. 
     * Override this method for each command to produce the desired formatted 
     * command result for the report.
     */
    public void addToReport()
    {
        
    }
}