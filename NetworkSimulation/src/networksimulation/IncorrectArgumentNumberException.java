package networksimulation;

/**
 * Version: 1
 * Date Created: 02/01/2020
 * Author: Jack Warner-Pankhurst, University of East Anglia, 100199337
 * Description: This exception is called when the user attempts to create a 
 * command with too few or too many arguments
 */
public class IncorrectArgumentNumberException extends Exception {

    /**
     * Creates a new instance of <code>IncorrectArgumentsNumber</code> without
     * detail message.
     */
    public IncorrectArgumentNumberException() {
    }

    /**
     * Constructs an instance of <code>IncorrectArgumentsNumber</code> with the
     * specified detail message.
     *
     * @param msg the detail message.
     */
    public IncorrectArgumentNumberException(String msg) {
        super(msg);
    }
}
