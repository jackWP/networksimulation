package networksimulation;

/**
 * Version: 1
 * Date Created: 02/01/2020
 * Author: Jack Warner-Pankhurst, University of East Anglia, 100199337
 * Description: This exception is called when the user attempts to use the wrong
 * data type for a command argument, such as a String instead of an Integer
 */
public class IncorrectArgumentTypeException extends Exception {

    /**
     * Creates a new instance of <code>IncorrectArgumentTypeException</code>
     * without detail message.
     */
    public IncorrectArgumentTypeException() {
    }

    /**
     * Constructs an instance of <code>IncorrectArgumentTypeException</code>
     * with the specified detail message.
     *
     * @param msg the detail message.
     */
    public IncorrectArgumentTypeException(String msg) {
        super(msg);
    }
}
