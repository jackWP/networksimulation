package networksimulation;

/**
 * Version: 1
 * Date Created: 02/01/2020
 * Author: Jack Warner-Pankhurst, University of East Anglia, 100199337
 * Description: This exception is called when the BasicNodes HashMap in the 
 * network class is found to be empty before a method is called on it.
 * Prevents commands or incidents executing on nothing
 */
public class EmptyNodeHashMapException extends Exception {

    /**
     * Creates a new instance of <code>EmptyNodeHashMapException</code> without
     * detail message.
     */
    public EmptyNodeHashMapException() {
    }

    /**
     * Constructs an instance of <code>EmptyNodeHashMapException</code> with the
     * specified detail message.
     *
     * @param msg the detail message.
     */
    public EmptyNodeHashMapException(String msg) {
        super(msg);
    }
}
