package networksimulation;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;

/**
 * Version: 1
 * Date Created:
 * Author: Jack Warner-Pankhurst, University of East Anglia, 100199337
 * Description: This class is used to store the BasicNoes in the Graph, and link 
 * them together through their stored adjacent nodes. They also store their 
 * distance from the source node, and the shortest path to get to it.
 */
public class GraphNode 
{
    /**
     * The BasicNode that the GraphNode represents in the Graph
     */
    private BasicNode networkBasicNode;
     
    /**
     * The stored shortest path that is calculated by the Graph it is stored in
     */
    private LinkedList<GraphNode> shortestPath = new LinkedList<>();
     
    /**
     * The initial distance in set to the maximum possible so it can be properly
     * evaluated for the shortest path to it
     */
    private int distance = Integer.MAX_VALUE;
     
    /**
     * Map of all of the nodes immediately connected nodes, or adjacencies 
     */
    private Map<GraphNode, Integer> adjacentNodes = new HashMap<>();
 
    public GraphNode(BasicNode n) 
    {
        this.networkBasicNode = n;
    }
    
    /**
     * Adds and adjacent node to the current map
     * @param destination graphNode to add
     * @param distance distance to it
     */
    public void addDestination(GraphNode destination, int distance) 
    {
        adjacentNodes.put(destination, distance);
    }
  
    /**
     * Returns the stored distance form the source node
     * @return Integer as the stored distance to the node
     */
    public int getDistance()
    {
        return this.distance;
    }
    
    /**
     * Sets the distance to this node from the source node
     * @param d Integer to set the distance to
     */
    public void setDistance(int d)
    {
        this.distance = d;
    }
    
    /**
     * Sets the BasicNode that this GraphNode represents
     * @param n BasicNode to set the stored node as
     */
    public void setNode(BasicNode n)
    {
        this.networkBasicNode = n;
    }
    
    /**
     * Gets the BasicNode this GraphNode currently stores
     * @return The BasicNode currently stored
     */
    public BasicNode getNode()
    {
        return this.networkBasicNode;
    }
    
    /**
     * Gets the adjacent nodes stored by this GraphNode
     * @return Map<GraphNode, Integer> of the current adjacent GraphNodes
     */
    public Map<GraphNode, Integer> getAdjacentNodes()
    {
        return this.adjacentNodes;
    }
    
    /**
     * Sets the shortest path Linked List in this node
     * @param path LinkedList to set the shortest path to
     */
    public void setShortestPath(LinkedList<GraphNode> path)
    {
        this.shortestPath = path;
    }
    
    /**
     * Gets the shortest path Linked List in this node
     * @return the shortest path Linked List currently in this node
     */
    public LinkedList<GraphNode> getShortestPath()
    {
        return this.shortestPath;
    }
    
    /**
     * Creates a String representation of the GraphNode, including its BasicNode
     * ID, distance, and the stored shortest path
     * @return String representation of the GraphNode
     */
    @Override
    public String toString()
    {
        StringBuilder s = new StringBuilder();
        s.append("BasicNode ID: ");
        s.append(this.getNode().getID());
        s.append(" Distance to source node: ");
        s.append(this.getDistance());
        s.append(" Shortest path to source node is :");
        s.append("\n");
        s.append("Source");
        for(GraphNode n : this.getShortestPath())
        {
            s.append("<-");
            s.append(n.getNode().getID());
        }
        
        return s.toString();
    }
}