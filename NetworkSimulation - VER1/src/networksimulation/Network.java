package networksimulation;
import java.util.Calendar;

/**
 * Version: 1
 * Date Created: 02/01/2020
 * Author: Jack Warner-Pankhurst, University of East Anglia, 100199337
 * Description: Abstract Network object class. Use by inheriting and overriding
 * all provided methods to specify method functionality for specific network 
 * type. Network modifications could include modifiers, limitations, or rules 
 * that complicate or restrict functionalities for the user. This class serves 
 * as the basis for all the interactions in the simulation, by providing a class
 * to link nodes, connections, incidents, commands, and reporting in one place.
 * This class abstract as it only serves as a guide for network objects, all 
 * existing methods should be overridden to add the desired implementation.
 * 
 * Methods in this object to override:
 * generateNetwork
 * runNetwork
 * generateReport
 * generateCommandList
 * readCommands
 * readCommandFile
 * saveCommandFile
 * addToReport (may not need this)
 */
public abstract class Network 
{
    /**
     * Seed is set when network Object is created, used as a basis for all 
     * random generation later in network generation simulation. Final as value
     * should never need to be modified.
     */
    private final int seed;
    
    /**
     * Starts at time of object creation. Is used to create a simulated timer   ---------> see notes on possible changes
     * for events to be based off. A list of events will be created, either 
     * commands and incidents, then the simulation timer will be moved to the 
     * next event (whichever is closest) and the event performed. 
     */
    private Calendar simulationTime = Calendar.getInstance();
    
    /**
     * Network Constructor. Requires seed for network and incident generation.
     * No default constructor as a network can never be cerated with out a 
     * seed. Internal simulation time (simulationTime) starts from creation of
     * Network object.
     * @param seed derived from student ID to create generation seed for all
     * randomly generated elements of the network. Child classes should still set 
     */
    public Network(int seed)
    {
        this.seed = seed;
    }
    
    /**
     * generateNetwork method used to generate the network of the specified type
     * using the specified parameters (connections and incidents). Override in
     * child class to implement desired functionality.
     */
    public void generateNetwork()
    {
        
    }
    
    /**
     * runNetwork method used to run the simulation from the date specified on 
     * object creation. This method should use the specified connection type(s),
     * node type(s) and incident type(s) to build a network based on the seed. 
     * Override in child class to implement desired functionality.
     */
    public void runNetwork()
    {
        
    }
    
    /**
     * generateReport method used to format the raw log data into a format that
     * can be saved and later read by the user. This si left open for 
     * implementation as different commands are likely to produce different log 
     * data, and raw log data may need to be compressed (if at all) on a 
     * command basis. Override in child class to implement desired functionality.
     */
    public void generateReport()
    {
        
    }
    
    /**
     * generateCommandList method used to create command list from the read in 
     * command file. Returned format should provide desired action and a 
     * timestamp for the network to then run to and perform the specified 
     * action. Override in child class to implement desired functionality.
     */
    public void generateCommandList()
    {
        
    }
    
    /**
     * readComands method used to read in the command list and create a list of
     * commands usable by the network object to create a list of tasks. 
     * Override in child class to implement desired functionality.
     */
    public void readCommands()
    {
        
    }
    
    /**
     * readCommandFile reads the raw commands from the file provided by the user
     * so that they can be formatted for the network object. Override in child 
     * class to implement desired functionality.
     */
    public void readCommandFile()
    {
        
    }
    
    /**
     * saveCommandFile method takes the list of commands currently in the GUI 
     * commands text editing area and saves it to a file for the user. 
     * Override in child class to implement desired functionality.
     */
    public void saveCommandFile()
    {
        
    }
    
    /**
     * addToReport method adds a formatted action to the report so it can be 
     * saved and analysed by the user later. Override in child class to 
     * implement desired functionality.
     */
    public void addToReport()
    {
        
    }
    
    /**
     * Method to get the time the object was created that acts as the start for
     * the simulation time, returned as a Calendar object.
     * @return Calendar object that stores the start time for the simulation
     */
    public Calendar getSimulationTime()
    {
        return this.simulationTime;
    }
}