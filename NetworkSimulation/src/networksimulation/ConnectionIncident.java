package networksimulation;

import java.time.LocalDateTime;
import java.util.HashMap;

/**
 * Version: 1
 * Date Created: 02/01/2020
 * Author: Jack Warner-Pankhurst, University of East Anglia, 100199337
 * Description: This class is for simulating changes in the networks connections. 
 * It does this by setting the speed , either higher or lower, of the 
 * target connection at its execution time in the simulation
 */
public class ConnectionIncident extends Incident
{
    /**
     * Amount to change the targeted nodes processing speed by
     */
    private final int changeAmount;
    
    /**
     * ID of the targeted node
     */
    private final int targetNodeID;
    
    /*
     * Referance to the list of connections so it can be quieried
     */
    private HashMap<Integer, BasicConnection> connections;
    
    /**
     * Constructor for a NodeProcessingIncident object
     * @param ID Unique ID for the incident
     * @param time The execution time of the incident
     * @param change How much to change the node by
     * @param targetID The ID of the targeted node
     * @param connections The HashMap of connections used for executing the incident
     */
    public ConnectionIncident(int ID, LocalDateTime time, int change, int targetID, HashMap<Integer, BasicConnection> connections)
    {
        super(ID, time);
        this.changeAmount = change;
        this.connections = connections;
        this.targetNodeID = targetID;
    }
    
    /**
     * Gets the HahsMap currently stored in the incident
     * @return HashMap<Integer, BasiBasicConnectioncNode> currently stored by 
     * the incident
     */
    public HashMap<Integer, BasicConnection> getConnectionHashMap()
    {
        return this.connections;
    }
    
    /**
     * Gets the Integer ID of the incident
     * @return Integer ID of the incident
     */
    public int getTargetID()
    {
        return this.targetNodeID;
    }
    
    /**
     * Gets the amount the incident will change the connection by
     * @return Integer as the change amount
     */
    public int getChangeAmount()
    {
        return this.changeAmount;
    }
    
    /**
     * Sub-type polymorphised method to run the incident. Done by setting the 
     * target connections spped time
     */
    @Override
    public void runIncident() 
    {
        //get the target connection form the list of connections
        BasicConnection target = this.getConnectionHashMap().get(this.getTargetID());
        try //This causes a NullPointerException for an unknown reason
        {   //set the connection speed of the connection to chnage amount
            target.setConnectionSpeed(this.getChangeAmount()); 
        }
        catch(java.lang.NullPointerException ex) //print excpetion for logging
        {
            System.out.println(ex);
        }
    }

    /**
     * Allows for comparing two incidents by their execution time
     * @param incident incident to compare to
     * @return 1 for greater than, 0 for equal, -1 for less than
     */
    @Override
    public int compareTo(Incident incident) 
    {
        return this.getExecutionTime().compareTo(incident.getExecutionTime());
    }
    
    /**
     * Creates a string representation of the incident, including its target ID,
     * execution time, and change amount
     * @return String representation of the incident
     */
    @Override
    public String toString()
    {
        return "Connection incident targetting connection " + this.targetNodeID + " at "
                + this.getExecutionTime() + " changing by "  + this.changeAmount;
    }
}