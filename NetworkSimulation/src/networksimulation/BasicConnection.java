package networksimulation;

/**
 * Version: 1
 * Date Created: 02/01/2020
 * Author: Jack Warner-Pankhurst, University of East Anglia, 100199337
 * Description: A Basic Connection inherits from Connection. It implements a
 * start node stored as an integer id, as well as the speed of the connection.
 * This is used to simulate the bandwidth between the start and end node.
 */
public class BasicConnection extends Connection implements Comparable<BasicConnection>
{
    /**
     * Integer that represents the bandwidth of the connection that is used when
     * investigating the delay between two nodes in the network.
     */
    private int speed;
    
    /**
     * Integer ID that stores the id of the node the connection starts from.
     */
    private final int startNodeID;
    
    /**
     * Constructor for a BasicConnection object. A uniquely identifying integer 
     * id, end and start node id, and connection speed.
     * @param id Integer id that uniquely identifies the connection on the 
     * network
     * @param endid Integer ID of the end node this connection points to.
     * @param startid Integer ID of the node this connection starts from.
     * @param speed Integer that is used to simulate the bandwidth of the 
     * connection
     */
    BasicConnection(int id, int startid ,int endid, int speed)
    {
        super(endid, id);
        this.startNodeID = startid;
        if (this.speed + speed < 0) 
        {
            this.speed = 0;
        }
        else
        {
            this.speed = speed;
        }
    }
    
    /**
     * Constructor for a BasicConnection object. Requires a uniquely identifying integer 
     * id, start node id, and connection speed. As no end node id is specified, 
     * a code of -1 is used to indicate no end node was set (node id's will 
     * never be negative other than in this case)
     * @param id Integer id that uniquely identifies the connection on the 
     * network
     * @param startid Integer ID of the node this connection starts from.
     * @param speed Integer that is used to simulate the bandwidth of the 
     * connection
     */
    BasicConnection(int id, int startid, int speed)
    {
        super(id);
        this.startNodeID = startid;
        if (this.speed + speed < 0) 
        {
            this.speed = 0;
        }
        else
        {
            this.speed = speed;
        }
    }
    
    /**
     * Method to get the current speed of the connection
     * @return Integer that is the speed stored by the connection
     */
    public int getConnectionSpeed()
    {
        return this.speed;
    }
    
    /**
     * Method to set the speed of the connection
     * @param speed Integer that speed is going to be set to
     */
    public void setConnectionSpeed(int speed)
    {
        if (this.speed + speed < 0) 
        {
            this.speed = 0;
        }
        else
        {
            this.speed = speed;
        }
    }
    
    /**
     * 
     * @return 
     */
    public int getStartNodeID()
    {
        return this.startNodeID;
    }
    
    /**
     * Returns a string representation of the BasicConnection object
     * @return String representation of the BasicConnection object
     */
    @Override
    public String toString()
    {
        String str = "Connection ID: " + this.getConnectionID() + "\n" + 
                "Connection start node: " + this.startNodeID+"\n" +
                "End Node ID:"  +this.getEndNodeID() + "\n" + 
                "Connection speed: " + this.speed  + "\n";
        return str;
    }
    
    /**
     * Compares the speed of the given connection to the current connection
     * @param c BasicConnection to perform the speed comparison
     * @return 1 if the current connection has a greater speed, 0 if they are 
     * equal, or -1 if the given connection has a greater speed
     */
    @Override
    public int compareTo(BasicConnection c) {
        if(this.speed > c.getConnectionSpeed()) return 1;
        
        else if(this.speed == c.getConnectionSpeed()) return 0;
        
        else return -1;
    }
}