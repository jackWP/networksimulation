package networksimulation;
import java.util.Calendar;
import java.util.HashMap;
import java.util.ArrayList;
import java.util.Random;

/**
 * Version: 1
 * Date Created: 02/01/2020
 * Author: Jack Warner-Pankhurst, University of East Anglia, 100199337
 * Description:
 */

public class BasicNetwork extends Network
{
    private final Calendar startTime;
    private final Calendar endTime;
    private HashMap<Integer,BasicNode> nodes;
    private HashMap<Integer, BasicConnection> connections;
    private ArrayList<Command> commandList;
    private ArrayList<Incident> incidentList;
    private ArrayList<String> report;
    
    
    public BasicNetwork(int seed, Calendar end)
    {
        super(seed);
        this.startTime = super.getSimulationTime();
        this.endTime = end;
        nodes = new HashMap();
        connections = new HashMap();
        commandList = new ArrayList<>();
        incidentList = new ArrayList<>();
        report = new ArrayList<>();
    }
    
    @Override
    public void generateNetwork()
    {
        for(int i = 0; i < 20; i++)
        {
            BasicNode n = new BasicNode(i,i);
            this.addNode(n);
        }
        Random r = new Random();
        for (int i = 0; i < 50; i++) 
        {
            int start = r.ints(0,(20+1)).findFirst().getAsInt();
            int end = r.ints(0,(20+1)).findFirst().getAsInt();
            BasicConnection c = new BasicConnection(i,start,end,i);
            this.addConnection(c);
        }
        
    }
    
    @Override
    public void runNetwork()
    {
        
    }
    
    @Override
    public void generateReport()
    {
        
    }
    
    public void addNode(BasicNode n)
    {
        this.nodes.put(n.getID(), n);
    }
    
    public void addConnection(BasicConnection c)
    {
        this.connections.put(c.getConnectionID(),c);
    }
    
    @Override
    public void generateCommandList()
    {
        
    }
    
    @Override
    public void readCommands()
    {
        
    }
    
    @Override
    public void saveCommandFile()
    {
        
    }
    
    public void saveReport()
    {
        
    }
    
    public void performIncident()
    {
        
    }
    
    public void generateIncidents()
    {
        
    }
    
    @Override
    public void addToReport()
    {
        
    }
    
    public static void main(String[] args) 
    {
//        BasicConnection con = new BasicConnection(1,1,0);
//        //System.out.print(con.toString());
//        
//        BasicConnection y = new BasicConnection(2,1,2,1);
//        //System.out.print(y);
//        
//        con.setEndNodeID(2);
//        System.out.print(y.compareTo(con));
//        
//        BasicNode n = new BasicNode(1,1);
//        
//        for(int i = 0; i < 10; i ++)
//        {
//            BasicConnection c = new BasicConnection(i,i,1);
//            n.addConnection(c);
//        }
//        ArrayList<Connection> t = n.getConnections();
//        for(Connection c : t)
//        {
//            System.out.print(c.toString());
//        }
//        
//        BasicNode n1 = new BasicNode(1,0);
//        System.out.println(n.compareTo(n1));
        
        
        Calendar c = Calendar.getInstance(); 
        BasicNetwork n = new BasicNetwork(1,c);
        n.generateNetwork();
        
    }  
}
