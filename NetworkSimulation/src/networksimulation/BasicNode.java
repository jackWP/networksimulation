package networksimulation;
import java.util.ArrayList;

/**
 * Version: 1
 * Date Created: 02/01/2020
 * Author: Jack Warner-Pankhurst, University of East Anglia, 100199337
 * Description: BasicNodes serve to simulate access points on a network for 
 * packets to be routed through. 
 */
public class BasicNode extends Node implements Comparable<BasicNode>
{
    /**
     * This value simulates the amount of processing that node requires in order
     * to pass the simulated 'packet' onto the next node. A total of all 
     * processing time form all required nodes will go towards the overall 
     * travel time between nodes. The value can be increased or decreased 
     * to simulate changes in overall network traffic.
     */
    private int processingTime;
    
    /**
     * Standard constructor for a BasicNode object. Initialises it with a unique 
     * integer id to identify it, and a processing time to simulate packet 
     * processing time inside a node.
     * @param id Integer id to identify the node
     * @param processingTime Processing time the node will cost to pass through 
     */
    public BasicNode(int id, int processingTime)
    {
        super(id);
        if(this.processingTime - processingTime < 0)
        {
            this.processingTime = 0;
        }
        else
        {
            this.processingTime = processingTime;    
        }
    }
    
    /**
     * Sets the nodes processing time to the given int
     * @param time Integer to set the nodes processing time value to
     */
    public void setProcessingTime(int time)
    {
        if(time < 0)
        {
            this.processingTime = 0;
        }
        else
        {
            this.processingTime = time;    
        }
    }
    
    /***
     * Returns the integer ID of the closest (shortest distance) connection
     * currently stored by the node
     * @return Integer ID of the shortest distance connection stored by the 
     * node
     */
    public int getClosestConnection()
    {
        int shortestConnectionID = -1;
        int currentShortestDistance = Integer.MAX_VALUE;
        
        for(BasicConnection c : this.getConnections())
        {
            if(currentShortestDistance > c.getConnectionSpeed())
            {
                shortestConnectionID = c.getConnectionID();
                currentShortestDistance = c.getConnectionSpeed();
            }
            currentShortestDistance = c.getConnectionSpeed();
        }       
        return shortestConnectionID;
    }
    
    /**
     * Returns the nodes current set processing time
     * @return Integer that is the nodes current processing time
     */
    public int getProcessingTime()
    {
        return this.processingTime;
    }
    
    /**
     * Overrides parent method to return an ArrayList<BasicConnection> instead
     * of a generic collection, of the nodes currently stored connections
     * @return ArrayList<BasicConnection> of the nodes currently stored 
     * connections
     */
    @Override
    public ArrayList<BasicConnection> getConnections()
    {
        ArrayList<BasicConnection> c = new ArrayList<>(super.getConnections());
        return c;
    }
    
    /**
     * Compares the processing time of the given basic node to the current node
     * @param n BasicNode to perform the speed comparison
     * @return 1 if the current node has a greater speed, 0 if they are equal,
     * or -1 if the given node has a greater speed
     */
    @Override
    public int compareTo(BasicNode n) 
    {
        if(this.processingTime > n.getProcessingTime()) return 1;
        
        else if(this.processingTime == n.getProcessingTime()) return 0;
        
        else return -1;
    }
    
    /**
     * Returns a formatted string representation of the BasicNode
     * @return String format of the BasicNode:
     * Node ID - Connection number - COnnection ID's 
     */
    @Override
    public String toString()
    {
        StringBuilder s = new StringBuilder();
        s.append("Node ID: ");
        s.append(super.getID());
        s.append(" Connection num: ");
        s.append(this.getConnections().size());
        s.append(" Node processing speed: ");
        s.append(this.processingTime);
        s.append(" Connection ID's: ");
        for(BasicConnection c : this.getConnections())
        {
            s.append(c.getConnectionID());
            s.append(", ");
        }
        return s.toString();
    }
}