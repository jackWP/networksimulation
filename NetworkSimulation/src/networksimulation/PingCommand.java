package networksimulation;
import java.time.LocalDateTime;

/**
 * Version: 1
 * Date Created: 02/01/2020
 * Author: Jack Warner-Pankhurst, University of East Anglia, 100199337
 * Description: This class is used to query the network to simulate a ping 
 * command. It does this by getting the distance variable from the target 
 * graph node in the Graph at the execution time during the simulation
 */
public class PingCommand extends Command
{
    /**
     * Stores the targeted node so it can retrieve the distance to it
     */
    private final GraphNode endNode;
    
    /**
     * Stores the starting node as static, as this the same for all commands
     * and never changes
     */
    private static GraphNode startNode;
    
    /**
     * Store reference to the Graph to access the paths and shortest distances
     */
    private Graph networkGraph;
    
    /**
     * Constructor for a PingCommand object
     * @param id Unique ID to identify each command
     * @param executionTime When the command will be executed
     * @param start Node where the command will start from
     * @param end Where the command is targeting
     * @param networkGraph Graph needed to access the shortest paths and 
     * distances
     */
    public PingCommand(int id, LocalDateTime executionTime, GraphNode start, GraphNode end, Graph networkGraph)
    {
        super(id,executionTime);
        PingCommand.startNode = start;
        this.endNode = end;
        this.networkGraph = networkGraph;
    }

    /**
     * Sub-Type polymorphised method to run the command. Performs the Ping 
     * command by getting the distance from the start node to the end node and 
     * returns it
     * @return Integer representing the distance to the targeted node
     */
    @Override
    public String runCommand()
    {
        this.networkGraph.ShortestPath(PingCommand.startNode);
        int distance = this.endNode.getDistance();
        String result =  "Ping at "  + this.getExecutionTime().toString() + 
                " from " + PingCommand.startNode.getNode().getID() + " to " + 
                this.endNode.getNode().getID() + " had a total distance of " 
                + distance;
        StringBuilder builder = new StringBuilder();
        builder.append(result);
        builder.append("\n");
        return builder.toString(); 
    }
    
    /**
     * Sets the stored graph of the node
     * @param g Graph to set the nodes graph to
     */
    public void setGraph(Graph g)
    {
        this.networkGraph = g;
    }
    
    /**
     * Creates a string representation of the command, including its ID, start 
     * and end points
     * @return String representation of the command
     */
    @Override
    public String toString()
    {
        return "PING: " + super.toString()+ "\n" + 
                "Start node: " + PingCommand.startNode.getNode().getID() + 
                " End node: " + this.endNode.getNode().getID()+ "\n";
    }

    /**
     * Allows the comparison of two Command objects by their execution time
     * @param command The Command Object to compare to
     * @return 1 for greater than, 0 for equal, or -1 for less than
     */
    @Override
    public int compareTo(Command command) 
    {
        return this.getExecutionTime().compareTo(command.getExecutionTime());
    }
}
