package networksimulation;

import java.time.LocalDateTime;

/**
 * Version: 1
 * Date Created: 02/01/2020
 * Author: Jack Warner-Pankhurst, University of East Anglia, 100199337
 * Description: Abstract Network object class. Use by inheriting and overriding
 * provided methods to specify method functionality for specific network 
 * type. Network modifications could include modifiers, limitations, or rules 
 * that complicate or restrict functionalities for the user. This class serves 
 * as the basis for all the interactions in the simulation, by providing a class
 * to link nodes, connections, incidents, commands, and reporting in one place.
 * This class abstract as it only serves as a guide for network objects, all 
 * existing methods should be overridden to add the desired implementation.
 * 
 * Methods in this object to override:
 * generateNetwork
 * runNetwork
 * readCommandFile
 * addToReport
 */
public abstract class Network 
{
    /**
     * Seed is set when network Object is created, used as a basis for all 
     * random generation later in network generation simulation. Final as value
     * should never need to be modified.
     */
    private final int seed;
    
    /**
     * Starts at time of object creation. Is used to create a simulated timer   ---------> see notes on possible changes
     * for events to be based off. A list of events will be created, either 
     * commands and incidents, then the simulation timer will be moved to the 
     * next event (whichever is closest) and the event performed. 
     */
    private final LocalDateTime simulationStartTime = LocalDateTime.now();
    
    /**
     * Network Constructor. Requires seed for network and incident generation.
     * No default constructor as a network can never be cerated with out a 
     * seed. Internal simulation time (simulationTime) starts from creation of
     * Network object.
     * @param seed derived from student ID to create generation seed for all
     * randomly generated elements of the network. Child classes should still set 
     */
    public Network(int seed)
    {
        this.seed = seed;
    }
    
    /**
     * generateNetwork method used to generate the network of the specified type
     * using the specified parameters (connections and incidents). Override in
     * child class to implement desired functionality.
     */
    public abstract void generateNetwork();
    
    /**
     * runNetwork method used to run the simulation from the date specified on 
     * object creation. This method should use the specified connection type(s),
     * node type(s) and incident type(s) to build a network based on the seed. 
     * Override in child class to implement desired functionality.
     */
    public abstract void runNetwork();
    
    /**
     * readCommandFile reads the raw commands from the file provided by the user
     * so that they can be formatted for the network object. Override in child 
     * class to implement desired functionality.
     */
    public abstract void readCommandFile();
    
    /**
     * getSeed methods returns the seed for the network as an integer
     * @return Integer containing the stored seed of a network set during 
     * instantiation
     */
    public int getSeed()
    {
        return this.seed;
    }
    
    /**
     * Method to get the time the object was created that acts as the start for
     * the simulation time, returned as a LocalDateTime object.
     * @return LocalDateTime object that stores the start time for the simulation
     */
    public LocalDateTime getSimulationStartTime()
    {
        return this.simulationStartTime;
    }
}