package networksimulation;

/**
 * Version: 1
 * Date Created: 02/01/2020
 * Author: Jack Warner-Pankhurst, University of East Anglia, 100199337
 * Description:
 */
public class BasicNode extends Node implements Comparable<BasicNode>
{
    /**
     * This value simulates the amount of processing that node requires in order
     * to pass the simulated 'packet' onto the next node. A total of all 
     * processing time form all required nodes will go towards the overall 
     * travel time between nodes. The value can be increased or decreased 
     * to simulate changes in overall network traffic.
     */
    private int ProcessingTime;
    
    /**
     * Standard constructor for a BasicNode object. Initialises it with a unique 
     * integer id to identify it, and a processing time to simulate packet 
     * processing time inside a node.
     * @param id Integer id to identify the node
     * @param processingTime Processing time the node will cost to pass through 
     */
    public BasicNode(int id, int processingTime)
    {
        super(id);
        this.ProcessingTime = processingTime;
        
    }
    
    /**
     * Sets the nodes processing time to the given int
     * @param time Integer to set the nodes processing time value to
     */
    public void setProcessingTime(int time)
    {
        this.ProcessingTime = time;
    }
    
    /**
     * Returns the nodes current set processing time
     * @return Integer that is the nodes current processing time
     */
    public int getProcessingTime()
    {
        return this.ProcessingTime;
    }
    
    /**
     * Compares the processing time of the given basic node to the current node
     * @param n BasicNode to perform the speed comparison
     * @return 1 if the current node has a greater speed, 0 if they are equal,
     * or -1 if the given node has a greater speed
     */
    @Override
    public int compareTo(BasicNode n) {
        if(this.ProcessingTime > n.getProcessingTime()) return 1;
        
        else if(this.ProcessingTime == n.getProcessingTime()) return 0;
        
        else return -1;
    }
}