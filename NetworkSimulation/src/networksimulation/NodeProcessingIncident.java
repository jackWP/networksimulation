package networksimulation;

import java.time.LocalDateTime;
import java.util.HashMap;

/**
 * Version: 1
 * Date Created: 02/01/2020
 * Author: Jack Warner-Pankhurst, University of East Anglia, 100199337
 * Description: This class is for simulating changes in the networks nodes. It 
 * does this by setting the processing time, either higher or lower, of the 
 * target node at its execution time in the simulation
 */
public class NodeProcessingIncident extends Incident
{
    /**
     * Amount to change the targeted nodes processing speed by
     */
    private final int changeAmount;
    
    /**
     * ID of the targeted node
     */
    private final int targetNodeID;
    
    /*
     * Referance to the list of nodes so it canbe quieried
     */
    private HashMap<Integer, BasicNode> nodes;
    
    /**
     * Constructor for a NodeProcessingIncident object
     * @param ID Unique ID for the incident
     * @param time The execution time of the incident
     * @param change How much to change the node by
     * @param targetID The ID of the targeted node
     * @param nodes The HashMap of nodes used for executing the incident
     */
    public NodeProcessingIncident(int ID, LocalDateTime time, int change, int targetID, HashMap<Integer, BasicNode> nodes)
    {
        super(ID, time);
        this.changeAmount = change;
        this.nodes = nodes;
        this.targetNodeID = targetID;
    }
    
    /**
     * Gets the amount the incident will change the node by
     * @return Integer as the change amount
     */
    public int getChangeAmount()
    {
        return this.changeAmount;
    }
    
    /**
     * Gets the HahsMap currently stored in the incident
     * @return HashMap<Integer, BasicNode> currently stored by the incident
     */
    public HashMap<Integer, BasicNode> getNodeHashMap()
    {
        return this.nodes;
    }
    
    /**
     * Gets the Integer ID of the incident
     * @return Integer ID of the incident
     */
    public int getTargetID()
    {
        return this.targetNodeID;
    }

    /**
     * Sub-type polymorphised method to run the incident. Done by setting the 
     * target node's processing time
     */
    @Override
    public void runIncident() 
    {
        BasicNode target = this.getNodeHashMap().get(this.getTargetID());
        target.setProcessingTime(this.getChangeAmount());
    }

    /**
     * Allows for comparing two incidents by their execution time
     * @param incident incident to compare to
     * @return 1 for greater than, 0 for equal, -1 for less than
     */
    @Override
    public int compareTo(Incident incident) 
    {
        return this.getExecutionTime().compareTo(incident.getExecutionTime());
    }
    
    /**
     * Creates a string representation of the incident, including its target ID,
     * execution time, and change amount
     * @return String representation of the incident
     */
    @Override
    public String toString()
    {
        return "Node Processing Incident incident targetting connection " + 
                this.targetNodeID + " at " + this.getExecutionTime() + 
                " changing by "  + this.changeAmount;
    }
}