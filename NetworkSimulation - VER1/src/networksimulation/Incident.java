package networksimulation;
import java.util.Calendar;
/**
 * Version: 1
 * Date Created: 02/01/2020
 * Author: Jack Warner-Pankhurst, University of East Anglia, 100199337
 * Description: An Incident object serves to simulate changes in the network, 
 * such as connections dropping, or the change in connection speeds across the 
 * network. All they require is a Calendar object with a set time to act as a 
 * timer for when the incident to occur, and an identifying Integer id. The 
 * method to execute the incident needs to be short and fast, as many of them
 * will be executed sequentially, and they shouldn't individually make drastic 
 * changes to the network to better simulate the network conditions changing 
 * over time.
 * 
 * methods to override in this object:
 * performIncident
 */
public abstract class Incident 
{
    /**
     * Integer id that acts as a unique identifier for the Incident object on 
     * the network
     */
    private final int incidentID;
    
    /**
     * Calendar object that stores a set time from the networks creation for the 
     * incident to occur when it is processed by the network. 
     */
    private final Calendar executionTime;
    
    /**
     * Incident constructor for initialising the object with an unique id, and a
     * specified time (stored within a Calendar object) for the incident to 
     * occur. 
     * Incident objects should only ever be initialised with an id and 
     * occurrence time to prevent the creation of unidentifiable incidents with 
     * no set time for them to occur. 
     * @param id Integer id used to uniquely identify each incident object
     * @param c Calendar object that holds the specified time for the incident
     * on the network to occur relative to the networks initialisation. 
     */
    public Incident(int id, Calendar c)
    {
        this.incidentID = id;
        this.executionTime = c;
    }
    
    /**
     * Method to get the currently set occurrence time as a Calendar object
     * @return Calendar object current stored in the incident object
     */
    public Calendar getOccurenceTime()
    {
        return this.executionTime;
    }
    
    /**
     * Method that is intended to perform the intended effect on the network to 
     * simulate it changing over time due to natural changes. 
     * This method needs to be overridden to implement the desired effect on the
     * network, or the nodes and connections within it. Incidents need to be 
     * simple in their effect as many could need to be added to the processing 
     * list 
     */
    public void performIncident()
    {
        
    }
}