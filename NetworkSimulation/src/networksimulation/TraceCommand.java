package networksimulation;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.LinkedList;

/**
 * Version: 1
 * Date Created: 02/01/2020
 * Author: Jack Warner-Pankhurst, University of East Anglia, 100199337
 * Description: This class is used to query the network to simulate a trace 
 * command. It does this by getting the path variable from the target 
 * graph node in the Graph at the execution time during the simulation
 */
public class TraceCommand extends Command
{
    /**
     * Stores the targeted node so it can retrieve the distance to it
     */
    private final GraphNode endNode;
    
    /**
     * Stores the starting node as static, as this the same for all commands
     * and never changes
     */
    private final GraphNode startNode;
    
    /**
     * Store reference to the Graph to access the paths and shortest distances
     */
    private Graph networkGraph;

    /**
     * Constructor for a Trace Command object
     * @param id Unique ID to identify each command
     * @param executionTime When the command will be executed
     * @param start Node where the command will start from
     * @param end Where the command is targeting
     * @param networkGraph Graph needed to access the shortest paths and 
     * distances
     */
    public TraceCommand(int id, LocalDateTime executionTime, GraphNode start, GraphNode end, Graph networkGraph) 
    {
        super(id, executionTime);
        this.endNode = end;
        this.startNode = start;
        this.networkGraph = networkGraph;
    }

    /**
     * Sub-Type polymorphised method to run the command. Performs the trace 
     * command by getting the Path stored in the end node and returning a 
     * String version of it
     * @return String representing the shortest path to the end node
     */
    @Override
    public String runCommand()
    {
        this.networkGraph.ShortestPath(this.startNode);
        StringBuilder builder = new StringBuilder();
        LinkedList<GraphNode> shortestPath = this.startNode.getShortestPath();
        
        builder.append("Shortest path from node ");

        builder.append(this.startNode.getNode().getID());
        builder.append( " to node ");
        builder.append(this.endNode.getNode().getID());
        builder.append(" at ");
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
        builder.append(this.getExecutionTime().format(formatter));
        builder.append(" || ");
        builder.append(this.startNode.getNode().getID());
        for(GraphNode node : shortestPath)
        {
            builder.append("<-");
            builder.append(node.getNode().getID());
        }
        
        builder.append("<-");
        builder.append(this.endNode.getNode().getID());
        
        builder.append("\n");
        return builder.toString();
    }
    
    /**
     * Sets the stored graph of the node
     * @param g Graph to set the nodes graph to
     */
    public void setGraph(Graph g)
    {
        this.networkGraph = g;
    }

    /**
     * Creates a string representation of the command, including its ID, start 
     * and end points
     * @return String representation of the command
     */
    @Override
    public String toString()
    {
        return "TRACE: " + super.toString()+ "\n" + 
                "Start node: " + this.startNode.getNode().getID() + 
                "End node: " + this.endNode.getNode().getID()+ "\n";
    }
    
    /**
     * Allows the comparison of two Command objects by their execution time
     * @param command The Command Object to compare to
     * @return 1 for greater than, 0 for equal, or -1 for less than
     */
    @Override
    public int compareTo(Command command) 
    {
        return this.getExecutionTime().compareTo(command.getExecutionTime());
    }
}