package networksimulation;
import java.util.ArrayList;
import java.util.Collection;

/**
 * Version: 1
 * Date Created: 02/01/2020
 * Author: Jack Warner-Pankhurst, University of East Anglia, 100199337
 * Description: The purpose of a Node object is to be contained within some 
 * appropriate data structure within an implementation of the Network object. 
 * Nodes are used to simulate the different access points or devices that are 
 * connected to a network. The connection that are then associated with the 
 * nodes can then be sued to build the topology of the network. The 
 * implementation of the Node object can include limitations or certain rules 
 * for communicating with other nodes to simulate specific networks or network
 * conditions.
 * This class abstract as it only serves as a guide for node objects, but the 
 * existing basic methods can either be used as is or overridden for more 
 * complex implementations.
 * 
 * Methods to override in this object:
 * addConnection 
 * removeConnection 
 */
public abstract class Node 
{
    /**
     * Node nodeID must be set on creation of object, used to uniquely identify 
     * each node in the collection of nodes contained by the network object.
     */
    private final int nodeID;
    
    /**
     * ArrayList of connections that this node holds. Can be empty when node is
     * constructed and added to or removed from later. Nodes with no connections
     * should still be reachable through the list of nodes contained in the 
     * network object.
     */
    private ArrayList<Connection> connections;
    
    /**
     * Node constructor that takes an integer to set as the nodes nodeID. Nodes must
     * be created with an nodeID to avoid unreachable nodes on the network. 
     * Initialises with an empty connections list. 
     * @param id Integer that is used as the nodeID of the node
     */
    public Node(int id)
    {
        this.nodeID = id;
        connections = new ArrayList<>();
    }
    
    /**
     * Node constructor that takes an integer nodeID to set as the nodes nodeID, 
     * and a single connection. 
     * @param id Integer nodeID to set as the nodes nodeID
     * @param c single Connection to add to add to the nodes list of connections
     * on initialisation.
     */
    public Node(int id, Connection c)
    {
        this.nodeID = id;
        this.connections.add(c);
    }
    
    /**
     * Node constructor that initialises the node with an integer nodeID and an 
     * existing collection on Connection objects. 
     * existing 
     * @param id Integer to set the nodes nodeID as
     * @param c Existing collection of Connections to add to the nodes list of
     * connections
     */
    public Node(int id, Collection c)
    {
        this.nodeID = id;
        this.connections.addAll(c);
    }
    
    /**
     * Method to to get the current list of connections held by the node
     * @return Returns a ArrayList of type Connection containing the current
     * list of connections the node holds.
     */
    public ArrayList<Connection> getConnections()
    {
        return this.connections;
    }
    
    /**
     * Method to return the nodes nodeID as an integer
     * @return Integer that is the nodes nodeID
     */
    public int getID()
    {
        return this.nodeID;
    }
    
    /**
     * Method to return the current number of connections associated with a node
     * @return Integer of the number of nodes associated with the node.
     */
    public int getNumberOfConnections()
    {
        return this.connections.size();
    }
    
    /**
     * Method to add a connection object to the nodes list of connections
     * @param c Connection object to add to the nodes list 
     * @return Boolean value, true meaning the Connection was added successfully
     * false if not
     */
    public boolean addConnection(Connection c)
    {
        if(!this.doesConnectionExist(c))
        {
            this.connections.add(c);
            return true;
        }
        else
        {
            return false;
        }
    }
    
    /**
     * Method to remove the given Connection object from the Nodes list
     * @param c Connection object to remove form the Nodes list
     * @return Boolean value, true meaning the Connection was removed 
     * successfully, false if not
     */
    public boolean removeConnection(Connection c)
    {
        if(this.doesConnectionExist(c))
        {
            this.connections.remove(c);
            return true;
        }
        else
        {
            return false;
        }
    }
    
     /**
     * Method to check if a given Connection object exists within the list of 
     * Connections
     * @param c Connection object to check if it exists in the current ArrayList
     * @return Boolean, where true means the Connection does exist, false means
     * it does not
     */
    public boolean doesConnectionExist(Connection c)
    {
        ArrayList<Connection> conections_Temp = this.getConnections();
        
        return conections_Temp.contains(c);
    }
}