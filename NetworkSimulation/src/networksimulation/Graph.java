package networksimulation;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.Map;
import java.util.Set;

/**
 * Version: 1
 * Date Created:
 * Author: Jack Warner-Pankhurst, University of East Anglia, 100199337
 * Description: This class is sued to create a searchable version of the 
 * lists of BasicNodes and Connection for the commands to use. It implements
 * Dijkstra's shortest path algorithm to work out the distances between nodes
 */
public class Graph 
{   
    /**
     * HashMap of basicNodes and GraphNoeds to be able to find one from the 
     * other
     */
    private HashMap<BasicNode,GraphNode> nodes = new HashMap<>();
    
    /**
     * No explicit constructor as there are no explicit fields to initialise
     */
     
    /**
     * Adds a GraphNode to the hashmap, if it does not already exist
     * @param node GraphNode to add
     */
    public void addNode(GraphNode node) 
    {
        this.nodes.putIfAbsent(node.getNode(), node);
    }
 
    /**
     * Gets all of the BasicNodes (keys) from the hashmap
     * @return ArrayList<BasicNode> of all the BasicNodes
     */
    public ArrayList<BasicNode> getBasicNodes()
    {
        ArrayList<BasicNode> nodeArrayList = new ArrayList<>();
        nodeArrayList.addAll(this.nodes.keySet());
        return nodeArrayList;
    }
    
    /**
     * Gets all if the graphNodes (values) from the hashmap
     * @return ArrayList<GraphNode> of all the GraphNodes in the hashmap
     */
    public ArrayList<GraphNode> getGraphNodes()
    {
        Collection c = this.nodes.values();
        ArrayList<GraphNode> nodeArrayList = new ArrayList<>(c);
        return nodeArrayList;
    }
    
    /**
     * This is where Dijkstras Shortest Path algorithm is performed to get the 
     * shortest path, and distance to all nodes from the source node. 
     * @param source graph node to start from
     */
    public void ShortestPath(GraphNode source) 
    {
        source.setDistance(0); //sourve has no distance

        //use sets to ensure values are unique and nodes are not revisited
        Set<GraphNode> settledNodes = new HashSet<>(); //visited nodes
        Set<GraphNode> unsettledNodes = new HashSet<>(); //not visited nodes

        unsettledNodes.add(source); //start at source

        while (!unsettledNodes.isEmpty()) //while there are unvisited nodes
        {
            //get the closest node from the list of unsettled nodes
            GraphNode currentNode = this.getLowestDistanceNode(unsettledNodes);
            unsettledNodes.remove(currentNode); 
            //remove current node from unettled
            
            //for every adjacency a node has
            for(Map.Entry<GraphNode, Integer> adjacencyPair: 
                    currentNode.getAdjacentNodes().entrySet()) 
            {
                //get the weight of that adjacency
                GraphNode adjacentNode = adjacencyPair.getKey();
                Integer edgeWeight = adjacencyPair.getValue();
                //if that node is not already visited
                if (!settledNodes.contains(adjacentNode)) 
                {
                    //get the distance to that ndoe and add it to unvisited list
                    CalculateMinimumDistance(adjacentNode, edgeWeight, currentNode);
                    unsettledNodes.add(adjacentNode);
                }
            }
            //then add current node to visited
            settledNodes.add(currentNode);
        }
    }
    
    /**
     * Method to get the closest note from a Set of nodes
     * @param unsettledNodes Set of nodes to find the closest from
     * @return GraphNode with the lowest weight
     */
    private GraphNode getLowestDistanceNode(Set<GraphNode> unsettledNodes) 
    {
        GraphNode lowestDistanceNode = null;
        int lowestDistance = Integer.MAX_VALUE;
        
        for (GraphNode node: unsettledNodes) {
            int nodeDistance = node.getDistance();
            if (nodeDistance < lowestDistance) 
            {
                lowestDistance = nodeDistance;
                lowestDistanceNode = node;
            }
        }
        return lowestDistanceNode;
    }
    
    /**
     * Method to get the minimum distance between a node and the source node 
     * using its existing path to get to it
     * @param evaluationNode Node to evaluate distance to
     * @param edgeWeight Weight of the move to get to it
     * @param sourceNode Node to start from
     */
    private void CalculateMinimumDistance(GraphNode evaluationNode, int edgeWeight, GraphNode sourceNode) 
    {
        int sourceDistance = sourceNode.getDistance();
        
        if (sourceDistance + edgeWeight < evaluationNode.getDistance()) 
        {
            evaluationNode.setDistance(sourceDistance + edgeWeight);
            LinkedList<GraphNode> shortestPath = new LinkedList<>(sourceNode.getShortestPath());
            shortestPath.add(sourceNode);
            evaluationNode.setShortestPath(shortestPath);
        }
    }
    
    /**
     * Gets the number of BasicNodes (size) int eh ahsmap
     * @return Integer as the size of the hashmap
     */
    public int getSize()
    {
        return this.nodes.keySet().size();
    }
    
    /**
     * Gets the corresponding graphNode from a BasicNode
     * @param node BasicNode to get the GraphNode from
     * @return The GraphNode of the given BasicNode
     */
    public GraphNode GraphNodeFromNetworkNode(BasicNode node)
    {
        return this.nodes.get(node);
    }
    
    /**
     * Creates a string representation of the Graph
     * @return String representation of the graph
     */
    @Override
    public String toString()
    {
        StringBuilder s = new StringBuilder();
        
        for(GraphNode g : this.nodes.values())
        {
            s.append(g.toString());
            s.append("\n");
        }
        return s.toString();
    }
}