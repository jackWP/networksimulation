package networksimulation;

/**
 * Version: 1
 * Date Created: 02/01/2020
 * Author: Jack Warner-Pankhurst, University of East Anglia, 100199337
 * Description: A Connection serves to simulate the link between nodes. A node
 * can have an start and end point, and any modifiers for "packet processing" to 
 * simulate connections between devices, routers etc. 
 * 
 * Methods to override in this object:
 * 
 */
public abstract class Connection 
{
    /**
     * Integer id that is the ID of the node this connection points to so that 
     * you can follow connections to their end points. ID is final as a 
     * connection's end point should not be modified, the unneeded object should
     * be deleted and a new one created with the new desired end point. This
     * should reduce the chances of duplicate connections pointing from the same
     * starting node to the same end node.
     */
    private int endNodesID;
    
    private final int connectionID;
    
    /**
     * Connection constructor that initialises the object with the integer id 
     * of the node the Connection object should point to, and the uniquely 
     * identifying id of the connection.
     * @param endid Integer id of the node this connection points to
     * @param id Integer id that uniquely identifies the connection on the 
     * network
     */
    public Connection(int endid, int id)
    {
        this.connectionID = id;
        this.endNodesID = endid;
    }
    
    /**
     * Connection constructor that initialises the object with a uniquely 
     * identifying id of the connection, and -1 as the code for no end node
     * @param id Integer id that uniquely identifies the connection on the 
     * network
     */
    public Connection(int id)
    {
        this.connectionID = id;
        this.endNodesID = -1;
    }
    
    /**
     * Sets the Integer end node id for this node
     * @param id Integer to set this nodes stored end node id to
     */
    public void setEndNodeID(int id)
    {
        this.endNodesID = id;
    }
    
    /**
     * Returns the integer id this connection holds
     * @return Integer id that is the id of the node this connection points to
     */
    public int getEndNodeID()
    {
        return this.endNodesID;
    }
    
    /**
     * Method to get the unique id of the connection
     * @return Integer id of the connection
     */
    public int getConnectionID()
    {
        return this.connectionID;
    }
}